# parjs combinators

## Characters

digit			ASCII(?) digit in <base>
hex			ASCII(?) digit in base 16 (hex)
uniDecimal		unicode digit in base 10 (decimal)

letter			ASCII letter
uniLetter		unicode letter

lower			ASCII lower-case letter
uniLower		unicode lower-case letter

upper			ASCII upper-case letter
uniUpper		unicode upper-case letter

space			single ASCII space
spaces1			one or more ASCII spaces (ie. space+)
whitespace		zero or more ASCII "whitespace characters", whatever that means

anyChar			a single character of any kind
anyCharOf		a single character in the specified <set: string>
noCharOf		a single character NOT in the specified <set: string>

## Control flow

replaceState		apply the specified parser, but within an isolated <userState> scope (which may also be specified as a function(parentState) -> scopedState)
backtrack		apply the specified parser and return the result, but do not advance the parser position
later			placeholder parser that does nothing, and on which the `.init` method needs to be called later, to (mutably!) fill in the actual logic

between			apply the <before: parser>, and then the specified parser, and then <after: parser>, and return the result of the middle one
thenq			apply the specified parser and then the <next> parser, and return the result of the former
qthen			apply the specified parser and then the <next> parser, and return the result of the latter
then			apply the specified parser and then the <next> parser, and return array containing the results of the [former, latter]
thenPick		apply the specified parser and then call the <selector: function(result, userState)>, which dynamically returns the next parser to apply

## Boolean operations

not			invert the result of the specified parser
or			attempt each of the specified <parsers ...> until one succeeds or all fail

## Repeats

exactly			apply the specified parser <count> times, and return an array of results
many			apply the specified parser until it runs out of matches, and return an array of results
manyBetween		apply <start: parser>, then the specified parser, until the <end: parser> is encountered, then call <projection: function(results[], end, userState)>
manySepBy		apply the specified parser until it runs out of matches, and expect each match to be separated with <delimiter: parser>; return an array of results
manyTill		apply the specified parser until the <end: parser> is encountered, and return the results of the specified parser

## Result and state handling

must			apply the specified parser, and expect its result to pass <predicate: function(result)>
mustCapture		apply the specified parser, and expect the parser position to have advanced

each			apply the specified parser, then call <projection: function(result, userState)>, and return the *original* result (userState may be mutated)
map			apply the specified parser, then call <projection: function(result, userState)>, and return the result of the projection function
mapConst		apply the specified parser, then throw away the result and return <constantResult> instead
maybe			apply the specified parser, and return its result if it succeeds, or <constantResult> otherwise (ie. a default value)

flatten			like map((result) => result.flat())
stringify		like map((result) => String(result)), but with extra stringification logic

## Error handling

recover			apply the specified parser, and if it hard-fails, call <recovery: function(failureState)> and, if a new parserState is returned from it, return that instead of the original